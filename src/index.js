import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App/App';
import * as serviceWorker from './serviceWorker';

import banana from "./images/banana.png";
import apple from "./images/apple.png";
import strawberry from "./images/strawberry.png";
import broccoli from "./images/broccoli.png";

let productItems = [
    {order: 1, name: "Banana", image: banana, price: 350},
    {order: 2, name: "Apple", image: apple, price: 300},
    {order: 3, name: "Strawberry", image: strawberry, price: 2000},
    {order: 4, name: "Broccoli", image: broccoli, price: 500}
];

ReactDOM.render(<App items={productItems}/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
