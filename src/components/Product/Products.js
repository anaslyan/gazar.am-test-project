import React, {Component} from "react";
import {Card} from "antd";
import {Row, Col, Input, Button, Icon} from "antd";

import styles from "./Products.css";

class Products extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [
                ...props.items
            ],
            itemSearch: "",
            purchasedItems: [],
            productList: {}
        };
    }

    handleChange = (e) => {
        this.setState({itemSearch: e.target.value});
    };

    onDrag = (e, item) => {
        e.preventDefault();
        this.setState({
            productList: item
        });
    };

    onDragOver = (e) => {
        e.preventDefault();
    };

    onDrop = (e) => {
        const {purchasedItems, productList, items} = this.state;

        this.setState({
            purchasedItems: [...purchasedItems, productList],
            items: items.filter(items => items.order !== productList.order),
            productList: {},
        });
    };

    addBasket = (e, item) => {
        const {purchasedItems, items} = this.state;

        this.setState({
            purchasedItems: [...purchasedItems, item],
            items: items.filter(items => items.order !== item.order),
            productList: item,
        });
    };

    removeBasket = (e, item) => {
        const {purchasedItems, productList, items} = this.state;
        console.log(this.state, "remove");
        items.push(item);
        const currentItem = purchasedItems.map(itm => itm.order).indexOf(productList.order);
        purchasedItems.splice(currentItem, 1);
        this.setState({});
    };

    render() {
        let productItems = this.state.items;
        let itemSearch = this.state.itemSearch.toLowerCase().trim();

        if (itemSearch.length > 0) {
            productItems = productItems.filter(item => {
                return item.name.toLowerCase().match(itemSearch);
            });
        }

        const {purchasedItems} = this.state;

        return (
            <div className={styles.main}>
                <Card style={{"width":"100%"}}>
                    <div style={{marginBottom: 16}}>
                        <Input
                            type="text"
                            addonAfter={<Icon type="search"/>}
                            placeholder="Search"
                            value={this.state.itemSearch}
                            onChange={this.handleChange}
                        />
                    </div>
                    <br/>
                    {productItems.map(item => (
                        <Card
                            key={item.order}
                            draggable
                            onDrag={(event) => this.onDrag(event, item)}
                            className={styles.col}
                        >
                            <Row gutter={16}>
                                <Col span={6} className={styles.col}>
                                    <img src={item.image} alt={item.name} className={styles.img}/>
                                </Col>
                                <Col span={6} className={styles.col}>
                                    <p className={styles.text} style={{fontWeight: "bold"}}>{item.name}</p>
                                    <p className={styles.text}>Price: {item.price}</p>
                                </Col>
                                <Col span={12} className={styles.imgDiv}>
                                    <Col span={8}>
                                        <p className={styles.text}>Quantity</p>
                                    </Col>
                                    <Col span={16}>
                                        <Input type="number" defaultValue={0} min="0"/>
                                    </Col>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <Col span={24}>
                                        <Button
                                            onClick={(event) => this.addBasket(event, item)}
                                            type="primary"
                                            className={styles.button}>
                                            Add to Basket
                                            <Icon type="shopping-cart"/>
                                        </Button>
                                    </Col>
                                </Col>
                            </Row>
                        </Card>
                    ))}
                </Card>
                <Card
                    onDrop={event => this.onDrop(event)}
                    onDragOver={(event => this.onDragOver(event))}
                    style={{"width":"100%"}}
                    className={styles.col}
                >
                    <p>Basket Products</p>
                    {purchasedItems.map(item =>
                        <Card
                            key={item.order}
                            className={styles.col}
                        >
                            <Row gutter={16}>
                                <Col span={6} className={styles.col}>
                                    <img src={item.image} alt={item.name} className={styles.img}/>
                                </Col>
                                <Col span={6} className={styles.col}>
                                    <p className={styles.text} style={{fontWeight: "bold"}}>{item.name}</p>
                                    <p className={styles.text}>Price: {item.price}</p>
                                </Col>
                                <Col span={12} className={styles.imgDiv}>
                                    <Col span={8}>
                                        <p className={styles.text}>Quantity</p>
                                    </Col>
                                    <Col span={16}>
                                        <Input type="number" defaultValue={1} min="1"/>
                                    </Col>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <Col span={24}>
                                        <Button
                                            onClick={(event) => this.removeBasket(event, item)}
                                            type="primary"
                                            className={styles.button}>
                                            Remove from Basket
                                            <Icon type="shopping-cart"/>
                                        </Button>
                                    </Col>
                                </Col>
                            </Row>
                        </Card>
                    )}
                </Card>
            </div>
        );
    }
}

export default Products;