import React, {Component} from "react";
import {Row, Col} from "antd";

import styles from "./App.css";
import Products from "../Product/Products";
// import Purchased from "../Purchased/Purchased";

class App extends Component {
    render() {
        return (
            <div className={styles.app}>
                <Row>
                    <Col span={12}><Products items={this.props.items} /></Col>
                    {/*<Col span={12}><Purchased items={this.props.items} /></Col>*/}
                </Row>
            </div>
        );
    }
}

export default App;
