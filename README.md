# Gazar.am-test-project

### Prerequisites

You need to have **node** installed on your local machine

### Installing

First you need to install all node modules by running:

```
yarn install
```

### Running project
In the project directory, run:

### `yarn start`

To run the app:
Open [http://localhost:3000](http://localhost:3000)

Improve:
Remove from Basket